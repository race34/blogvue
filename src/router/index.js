import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: require('../components/layout.vue').default,
      children: [
      	{
      		path: '',
      		name: 'Index',
      		component: require('../pages/layout/index.vue').default
      	}
      ]
    }
  ]
})
