// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Vuetify from 'vuetify'
import Moment from 'vue-moment'
import Axios from 'axios'
import VeeValidate from 'vee-validate'
import 'vuetify/dist/vuetify.min.css'

Vue.config.productionTip = false

Vue.use(Vuetify)
Vue.use(Moment)
Vue.use(Axios)
Vue.use(VeeValidate)

Axios.defaults.headers.common = {
  'Accept': 'application/json',
  'Content-Type': 'application/json',
  'X-Requested-With': 'XMLHttpRequest'
}

Vue.prototype.$http = Axios

//axios.defaults.baseURL = `http://192.168.100.17`;
Axios.defaults.baseURL = 'http://localhost:80/blog_app'
//Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
